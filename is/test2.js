var canvas;
var ctx;

var start = -10;
var end = 10;
//var step = 0.1;
var ratio;

var userRatioX = 0;
var userRatioY = 0;
var autoRatio = true;
var yRatio = false;

var func;

var center = {
    x: 0,
    y: 0
}

document.addEventListener("DOMContentLoaded",onLoad);

function onLoad(){
	canvas = document.getElementById('canvas2');
	canvas.onmousemove = onCtxMouseMove;
	window.onresize = onResize;
	ctx = canvas.getContext('2d');
	getParams();
	onResize();
}

function onResize(){
	canvas.width = window.innerWidth - 180;
	canvas.height = window.innerHeight - 60;
	center.x = Math.round(canvas.width/2);
	center.y = Math.round(canvas.height/2);
	ratio = canvas.width/(end-start);
	drawFunc();
}

function getParams(){
	try {
        //step = parseFloat(document.getElementById('step').value);
		end = parseFloat(document.getElementById('end2').value);
		start = parseFloat(document.getElementById('start2').value);
		ratio = canvas.width/(end-start);
		func = math.parse(document.getElementById('func2').value).compile();
		if(!autoRatio){
			userRatioX = parseFloat(document.getElementById('ratioX2').value);
			userRatioY = parseFloat(document.getElementById('ratioY2').value);
		}
	} catch (error) {
        console.log(error);
    }
}

let img;

function drawFunc(){
	let scope={
		x: 0
	}
	
	if(autoRatio){
		userRatioX=ratio;
		userRatioY=ratio;
	}
	center.x = Math.round((-start)*userRatioX);
	drawLine();
	
	ctx.strokeStyle = "#0000ff";
	ctx.fillStyle = "#0000ff";
    ctx.lineWidth = 1;
	ctx.beginPath();
	
	scope.x=Math.round(start);
    ctx.moveTo(start, func.evaluate(scope));
	while(scope.x<end){
		var y = func.evaluate(scope);
		ctx.beginPath();
		ctx.arc((scope.x-start)*userRatioX, center.y-(y*userRatioY),3,0,2*Math.PI);
		ctx.fill();
		scope.x=scope.x+1;
		ctx.stroke();
	}
	//
	img = ctx.getImageData(0,0,canvas.width,canvas.height);
}

function drawLine() {
    ctx.beginPath();
    ctx.strokeStyle = "black";
    ctx.lineWidth = 2;
    ctx.moveTo(center.x, 5);
    ctx.lineTo(center.x, canvas.height - 10);
    ctx.stroke();
    ctx.moveTo(5, center.y);
    ctx.lineTo(canvas.width - 10, center.y);
    ctx.stroke();
	
	ctx.beginPath();
    ctx.strokeStyle = "black";
    ctx.lineWidth = 0.5;
	var x = center.x + userRatioX;
	while(x<canvas.width){
		ctx.moveTo(x, 5);
		ctx.lineTo(x, canvas.height - 10);
		x = x + userRatioX;
	}
	while(x>0){
		ctx.moveTo(x, 5);
		ctx.lineTo(x, canvas.height - 10);
		x = x - userRatioX;
	}
	var y = center.y;
	while(y<canvas.height){
		ctx.moveTo(5, y);
		ctx.lineTo(canvas.width-10, y);
		y = y + userRatioY;
	}
	while(y>0){
		ctx.moveTo(5, y);
		ctx.lineTo(canvas.width-10, y);
		y = y - userRatioY;
	}
	ctx.stroke();
}

function onBtnClick(btn) {
	switch(btn.id){
		case "btnDraw2":
		    //event.preventDefault();
			ctx.clearRect(0, 0, canvas.width, canvas.height);
			getParams();
			//drawLine();
			drawFunc();
		break;
	}	
}



function onCtxMouseMove(event){
	if(func == null)
		return;
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	ctx.putImageData(img,0,0);
	ctx.strokeStyle = "#ff0000"
	ctx.lineWidth = 3;
	ctx.beginPath();
	var scope={
		x: 0
	}
	scope.x=Math.round((event.offsetX*(end-start)/canvas.width)+start);
    var y = func.evaluate(scope);
	//ctx.fill();
	document.getElementById('limy2').innerHTML = "lim(f(x)) = " + Math.round(scope.x*100)/100;
	document.getElementById('limx2').innerHTML = "x-> " + Math.round(y*100)/100;
	ctx.arc((scope.x-start)*userRatioX, center.y-(y*userRatioY),4,0,2*Math.PI);
    ctx.stroke();
}

function onInputChange(input) {
	switch(input.id){
		case "ratioX2":
		    
		break;
		case "ratioAutoCheckBox":
			autoRatio = input.checked;
			if(autoRatio){
				document.getElementById('ratioX2').disabled=true;
				document.getElementById('ratioY2').disabled=true;
				document.getElementById('ratioCheckBox2').disabled=true;
			}else{
				document.getElementById('ratioX2').disabled=false;
				document.getElementById('ratioY2').disabled=false;
				document.getElementById('ratioCheckBox2').disabled=false;
			}
			document.getElementById('ratioCheckBox2').checked=false;
			document.getElementById('ratioY2').value = ratio;
			document.getElementById('ratioX2').value = ratio;
		break;
	}	
}