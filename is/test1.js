var graf1, graf2, graf3;

document.addEventListener("DOMContentLoaded",onLoad);

function onLoad(){
	graf1 = new funcDrawer(document.getElementById("table1"));
	graf2 = new posledDrawer(document.getElementById("table2"));
	graf3 = new neprDrawer(document.getElementById("table3"));
	window.onresize = onResize;
}

function onResize(){
	graf1.onResize();
	graf2.onResize();
	graf3.onResize();
}

function onBtnClick(event){
	event.preventDefault();
	switch(event.currentTarget.parentElement.parentElement.parentElement.parentElement.id){
		case "table1":
			graf1.onBtnDraw();
		break;
		case "table2":
			graf2.onBtnDraw();
		break;
		case "table3":
			graf3.onBtnDraw();
		break;
	}	
}

function onInputChangeGlobal(event) {
	switch(event.currentTarget.parentElement.parentElement.parentElement.parentElement.id){
		case "table1":
			graf1.onInputChange(event.currentTarget);
		break;
		case "table2":
			graf2.onInputChange(event.currentTarget);
		break;
		case "table3":
			graf3.onInputChange(event.currentTarget);
		break;
	}	
}

function onCtxMouseMove(event){
	switch(event.currentTarget.parentElement.parentElement.parentElement.parentElement.id){
		case "table1":
			graf1.onCtxMouseMove(event);
		break;
		case "table2":
			graf2.onCtxMouseMove(event);
		break;
	}
}

function onCtxMouseDown(event){
		switch(event.currentTarget.parentElement.parentElement.parentElement.parentElement.id){
		case "table3":
			graf3.onCtxMouseDown(event);
		break;
	}
}

class funcDrawer{
	
	constructor(parentElement){
		this.canvas = parentElement.querySelector(".canvas");
		this.ctx = this.canvas.getContext('2d');
		this.parentElement = parentElement;
		this.autoRatio = true;
		this.center = {
			x: 0,
			y: 0
		}
		
		this.parentElement.querySelector('.btnDraw').onclick = onBtnClick;
		this.parentElement.querySelector('.ratioAutoCheckBox').onchange = onInputChangeGlobal;
		this.canvas.onmousemove = onCtxMouseMove;
		
		this.getParams();
		this.onResize();
	}
	
	onResize(){
		this.canvas.width = window.innerWidth - 180;
		this.canvas.height = window.innerHeight - 60;
		this.center.x = Math.round(this.canvas.width/2);
		this.center.y = Math.round(this.canvas.height/2);
		this.ratio = this.canvas.width/(this.end-this.start);
		this.drawFunc();
	}
	
	getParams(){
		try {
			this.step = parseFloat(this.parentElement.querySelector('.step').value);
			this.end = parseFloat(this.parentElement.querySelector('.end').value);
			this.start = parseFloat(this.parentElement.querySelector('.start').value);
			this.ratio = this.canvas.width/(this.end-this.start);
			this.func = math.parse(this.parentElement.querySelector('.func').value).compile();
			if(!this.autoRatio){
				this.userRatioX = parseFloat(this.parentElement.querySelector('.ratioX').value);
				this.userRatioY = parseFloat(this.parentElement.querySelector('.ratioY').value);
			}
		} catch (error) {
			console.log(error);
		}
	}
		
	drawFunc(){
		let scope={
			x: 0
		}
		
		if(this.autoRatio){
			this.userRatioX=this.ratio;
			this.userRatioY=this.ratio;
		}
		this.center.x = Math.round((-this.start)*this.userRatioX);
		this.drawLine();
		
		this.ctx.strokeStyle = "#0000ff";
		this.ctx.lineWidth = 1;
		this.ctx.beginPath();
		
		scope.x=this.start;
		this.ctx.moveTo(this.start, this.func.evaluate(scope));
		while(scope.x<this.end){
			var y = this.func.evaluate(scope);
			this.ctx.lineTo((scope.x-this.start)*this.userRatioX, this.center.y-(y*this.userRatioY));
			scope.x = scope.x + this.step;
		}
		this.ctx.stroke();
		this.img = this.ctx.getImageData(0,0,this.canvas.width,this.canvas.height);
		this.parentElement.querySelector('.ratioY').value = this.userRatioY;
		this.parentElement.querySelector('.ratioX').value = this.userRatioX;
	}
	
	drawLine() {
		if(this.userRatioX<=0)
			return;
		this.ctx.beginPath();
		this.ctx.strokeStyle = "black";
		this.ctx.lineWidth = 2;
		this.ctx.moveTo(this.center.x, 5);
		this.ctx.lineTo(this.center.x, this.canvas.height - 10);
		this.ctx.stroke();
		this.ctx.moveTo(5, this.center.y);
		this.ctx.lineTo(this.canvas.width - 10, this.center.y);
		this.ctx.stroke();
		
		this.ctx.beginPath();
		this.ctx.strokeStyle = "black";
		this.ctx.lineWidth = 0.5;
		var x = this.center.x + this.userRatioX;
		while(x<this.canvas.width){
			this.ctx.moveTo(x, 5);
			this.ctx.lineTo(x, this.canvas.height - 10);
			x = x + this.userRatioX;
		}
		while(x>0){
			this.ctx.moveTo(x, 5);
			this.ctx.lineTo(x, this.canvas.height - 10);
			x = x - this.userRatioX;
		}
		var y = this.center.y;
		while(y<this.canvas.height){
			this.ctx.moveTo(5, y);
			this.ctx.lineTo(this.canvas.width-10, y);
			y = y + this.userRatioY;
		}
		while(y>0){
			this.ctx.moveTo(5, y);
			this.ctx.lineTo(this.canvas.width-10, y);
			y = y - this.userRatioY;
		}
		this.ctx.stroke();
	}

	onBtnDraw(){
		this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
		this.getParams();
		this.drawFunc();
	}
	
	onCtxMouseMove(event){
		if(this.func == null)
			return;
		this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
		this.ctx.putImageData(this.img,0,0);
		this.ctx.beginPath();
		var scope={
			x: 0
		}
		
		if(event.offsetX < (this.canvas.width*0.0125))
		{
			var y1;
			var y2;
			var lim = 89898989;
			for (var i = 1; ((i < 100000) && (lim == 89898989)); i++) {
				scope.x = this.start-i;
				y2 = y1;
				y1 = this.func.evaluate(scope);
				if(Math.abs(y1-y2)<0.000001)
					lim = Math.round(y1*100)/100;
			}
			if(lim == 89898989)
				lim =NaN;
			this.parentElement.querySelector('.limy').innerHTML = "lim(f(x)) = " + lim;
			this.parentElement.querySelector('.limx').innerHTML = "x-> -infinity";
		}
		else if(event.offsetX> (this.canvas.width*0.9875))
		{
			var y1;
			var y2;
			var lim = 89898989;
			for (var i = 1; ((i < 100000) && (lim == 89898989)); i++) {
				scope.x = this.end+i;
				y2 = y1;
				y1 = this.func.evaluate(scope);
				if(Math.abs(y1-y2)<0.000001)
					lim = Math.round(y1*100)/100;
			}
			if(lim == 89898989)
				lim =NaN;
			this.parentElement.querySelector('.limy').innerHTML = "lim(f(x)) = " + lim;
			this.parentElement.querySelector('.limx').innerHTML = "x-> +infinity";
		}
		else
		{
			scope.x=(event.offsetX*(this.end-this.start)/this.canvas.width)+this.start;
			var y = this.func.evaluate(scope);
			this.parentElement.querySelector('.limy').innerHTML = "lim(f(x)) = " + Math.round(y*100)/100;
			this.parentElement.querySelector('.limx').innerHTML = "x-> " + Math.round(scope.x*100)/100;
			this.ctx.arc((scope.x-this.start)*this.userRatioX, this.center.y-(y*this.userRatioY),4,0,2*Math.PI);
			this.ctx.stroke();
		}
	}
	
	onInputChange(input) {
		switch(input.className){
			case "ratioX":
				
			break;
			case "ratioAutoCheckBox":
				this.autoRatio = input.checked;
				if(this.autoRatio){
					this.parentElement.querySelector('.ratioX').disabled=true;
					this.parentElement.querySelector('.ratioY').disabled=true;
					this.parentElement.querySelector('.ratioCheckBox').disabled=true;
				}else{
					this.parentElement.querySelector('.ratioX').disabled=false;
					this.parentElement.querySelector('.ratioY').disabled=false;
					this.parentElement.querySelector('.ratioCheckBox').disabled=false;
				}
				this.parentElement.querySelector('.ratioCheckBox').checked=false;
			break;
		}	
	}
	
}

class posledDrawer{
	
	constructor(parentElement){
		this.canvas = parentElement.querySelector(".canvas");
		this.ctx = this.canvas.getContext('2d');
		this.parentElement = parentElement;
		this.autoRatio = true;
		this.center = {
			x: 0,
			y: 0
		}
		
		this.parentElement.querySelector('.btnDraw').onclick = onBtnClick;
		this.parentElement.querySelector('.ratioAutoCheckBox').onchange = onInputChangeGlobal;
		this.canvas.onmousemove = onCtxMouseMove;
		
		this.getParams();
		this.onResize();
	}
	
	onResize(){
		this.canvas.width = window.innerWidth - 180;
		this.canvas.height = window.innerHeight - 60;
		this.center.x = Math.round(this.canvas.width/2);
		this.center.y = Math.round(this.canvas.height/2);
		this.ratio = this.canvas.width/(this.end-this.start);
		this.drawFunc();
	}
	
	getParams(){
		try {
			this.end = parseFloat(this.parentElement.querySelector('.end').value);
			this.start = parseFloat(this.parentElement.querySelector('.start').value);
			this.ratio = this.canvas.width/(this.end-this.start);
			this.func = math.parse(this.parentElement.querySelector('.func').value).compile();
			if(!this.autoRatio){
				this.userRatioX = parseFloat(this.parentElement.querySelector('.ratioX').value);
				this.userRatioY = parseFloat(this.parentElement.querySelector('.ratioY').value);
			}
		} catch (error) {
			console.log(error);
		}
	}
		
	drawFunc(){
		let scope={
			x: 0
		}
		
		if(this.autoRatio){
			this.userRatioX=this.ratio;
			this.userRatioY=this.ratio;
		}
	
		this.center.x = Math.round((-this.start)*this.userRatioX);
		this.drawLine();
		
		this.ctx.strokeStyle = "#0000ff";
		this.ctx.fillStyle = "#0000ff";
		this.ctx.lineWidth = 1;
		this.ctx.beginPath();
		
		scope.x=Math.round(this.start);
		this.ctx.moveTo(this.start, this.func.evaluate(scope));
		while(scope.x<=this.end){
			var y = this.func.evaluate(scope);
			this.ctx.beginPath();
			this.ctx.arc((scope.x-this.start)*this.userRatioX, this.center.y-(y*this.userRatioY),3,0,2*Math.PI);
			this.ctx.fill();
			scope.x=scope.x+1;
			this.ctx.stroke();
		}
		
		this.img = this.ctx.getImageData(0,0,this.canvas.width,this.canvas.height);
		this.parentElement.querySelector('.ratioY').value = this.userRatioY;
		this.parentElement.querySelector('.ratioX').value = this.userRatioX;
	}
	
	drawLine() {
		if(this.userRatioX<=0)
			return;
		this.ctx.beginPath();
		this.ctx.strokeStyle = "black";
		this.ctx.lineWidth = 2;
		this.ctx.moveTo(this.center.x, 5);
		this.ctx.lineTo(this.center.x, this.canvas.height - 10);
		this.ctx.stroke();
		this.ctx.moveTo(5, this.center.y);
		this.ctx.lineTo(this.canvas.width - 10, this.center.y);
		this.ctx.stroke();
		
		this.ctx.beginPath();
		this.ctx.strokeStyle = "black";
		this.ctx.lineWidth = 0.5;
		var x = this.center.x + this.userRatioX;
		while(x<this.canvas.width){
			this.ctx.moveTo(x, 5);
			this.ctx.lineTo(x, this.canvas.height - 10);
			x = x + this.userRatioX;
		}
		while(x>0){
			this.ctx.moveTo(x, 5);
			this.ctx.lineTo(x, this.canvas.height - 10);
			x = x - this.userRatioX;
		}
		var y = this.center.y;
		while(y<this.canvas.height){
			this.ctx.moveTo(5, y);
			this.ctx.lineTo(this.canvas.width-10, y);
			y = y + this.userRatioY;
		}
		while(y>0){
			this.ctx.moveTo(5, y);
			this.ctx.lineTo(this.canvas.width-10, y);
			y = y - this.userRatioY;
		}
		this.ctx.stroke();
	}

	onBtnDraw(){
		this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
		this.getParams();
		this.drawFunc();
	}
	
	onCtxMouseMove(event){
		if(this.func == null)
			return;
		this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
		this.ctx.putImageData(this.img,0,0);
		this.ctx.strokeStyle = "#ff0000"
		this.ctx.lineWidth = 3;
		this.ctx.beginPath();
		var scope={
			x: 0
		}
		
		if(event.offsetX < (this.canvas.width*0.0125))
		{
			var y1;
			var y2;
			var lim = 89898989;
			for (var i = 1; ((i < 100000) && (lim == 89898989)); i++) {
				scope.x = Math.round(this.start-i);
				y2 = y1;
				y1 = this.func.evaluate(scope);
				if(Math.abs(y1-y2)<0.000001)
					lim = Math.round(y1*100)/100;
			}
			if(lim == 89898989)
				lim =NaN;
			this.parentElement.querySelector('.limy').innerHTML = "lim(f(x)) = " + lim;
			this.parentElement.querySelector('.limx').innerHTML = "x-> -infinity";
		}
		else if(event.offsetX> (this.canvas.width*0.9875))
		{
			var y1;
			var y2;
			var lim = 89898989;
			for (var i = 1; ((i < 100000) && (lim == 89898989)); i++) {
				scope.x = Math.round(this.end+i);
				y2 = y1;
				y1 = this.func.evaluate(scope);
				if(Math.abs(y1-y2)<0.000001)
					lim = Math.round(y1*100)/100;
			}
			if(lim == 89898989)
				lim =NaN;
			this.parentElement.querySelector('.limy').innerHTML = "lim(f(x)) = " + lim;
			this.parentElement.querySelector('.limx').innerHTML = "x-> +infinity";
		}
		else
		{
			scope.x=Math.round((event.offsetX*(this.end-this.start)/this.canvas.width)+this.start);
			var y = this.func.evaluate(scope);
			this.parentElement.querySelector('.limy').innerHTML = "lim(a(n)) = " + Math.round(y*100)/100;
			this.parentElement.querySelector('.limx').innerHTML = "n-> " + Math.round(scope.x*100)/100;
			this.ctx.arc((scope.x-this.start)*this.userRatioX, this.center.y-(y*this.userRatioY),4,0,2*Math.PI);
			this.ctx.stroke();
		}
	}
	
	onInputChange(input) {
		switch(input.className){
			case "ratioX":
				
			break;
			case "ratioAutoCheckBox":
				this.autoRatio = input.checked;
				if(this.autoRatio){
					this.parentElement.querySelector('.ratioX').disabled=true;
					this.parentElement.querySelector('.ratioY').disabled=true;
					this.parentElement.querySelector('.ratioCheckBox').disabled=true;
				}else{
					this.parentElement.querySelector('.ratioX').disabled=false;
					this.parentElement.querySelector('.ratioY').disabled=false;
					this.parentElement.querySelector('.ratioCheckBox').disabled=false;
				}
				this.parentElement.querySelector('.ratioCheckBox').checked=false;
			break;
		}	
	}
	
}

class neprDrawer{
	
	constructor(parentElement){
		this.canvas = parentElement.querySelector(".canvas");
		this.ctx = this.canvas.getContext('2d');
		this.parentElement = parentElement;
		this.autoRatio = true;
		this.center = {
			x: 0,
			y: 0
		}
		
		this.parentElement.querySelector('.btnDraw').onclick = onBtnClick;
		this.parentElement.querySelector('.ratioAutoCheckBox').onchange = onInputChangeGlobal;
		this.canvas.onmousedown = onCtxMouseDown;
		
		this.regionFlag = false;
		this.neprFlag = true;
		this.region = {
			x1: 0,
			x2: 0
		}
		
		this.getParams();
		this.onResize();
	}
	
	onResize(){
		this.canvas.width = window.innerWidth - 180;
		this.canvas.height = window.innerHeight - 60;
		this.center.x = Math.round(this.canvas.width/2);
		this.center.y = Math.round(this.canvas.height/2);
		this.ratio = this.canvas.width/(this.end-this.start);
		this.drawFunc();
	}
	
	getParams(){
		try {
			this.step = parseFloat(this.parentElement.querySelector('.step').value);
			this.end = parseFloat(this.parentElement.querySelector('.end').value);
			this.start = parseFloat(this.parentElement.querySelector('.start').value);
			this.ratio = this.canvas.width/(this.end-this.start);
			this.func1 = math.parse(this.parentElement.querySelector('.func1').value).compile();
			this.func2 = math.parse(this.parentElement.querySelector('.func2').value).compile();
			this.func3 = math.parse(this.parentElement.querySelector('.func3').value).compile();
			this.x1 = parseFloat(this.parentElement.querySelector('.x1').value);
			this.x2 = parseFloat(this.parentElement.querySelector('.x2').value);
			this.parentElement.querySelector('.x3').value = this.end;
			if(!this.autoRatio){
				this.userRatioX = parseFloat(this.parentElement.querySelector('.ratioX').value);
				this.userRatioY = parseFloat(this.parentElement.querySelector('.ratioY').value);
			}
		} catch (error) {
			console.log(error);
		}
	}
		
	drawFunc(){
		let scope={
			x: 0
		}
		
		if(this.autoRatio){
			this.userRatioX=this.ratio;
			this.userRatioY=this.ratio;
		}
		this.center.x = Math.round((-this.start)*this.userRatioX);
		this.drawLine();
		
		this.ctx.strokeStyle = "#0000ff";
		this.ctx.lineWidth = 1;
		this.ctx.beginPath();
		
		scope.x=this.start;
		this.ctx.moveTo(this.start, this.func1.evaluate(scope));
		while(scope.x<this.end){
			if(scope.x<this.x1){
				var y = this.func1.evaluate(scope);
			}
			else if(scope.x<this.x2)
			{
				var y = this.func2.evaluate(scope);
			}
			else
			{
				var y = this.func3.evaluate(scope);
			}
			this.ctx.lineTo((scope.x-this.start)*this.userRatioX, this.center.y-(y*this.userRatioY));
			scope.x = scope.x + this.step;
		}
		this.ctx.stroke();
		this.img = this.ctx.getImageData(0,0,this.canvas.width,this.canvas.height);
		this.parentElement.querySelector('.ratioY').value = this.userRatioY;
		this.parentElement.querySelector('.ratioX').value = this.userRatioX;
	}
	
	drawLine() {
		if(this.userRatioX<=0)
			return;
		this.ctx.beginPath();
		this.ctx.strokeStyle = "black";
		this.ctx.lineWidth = 2;
		this.ctx.moveTo(this.center.x, 5);
		this.ctx.lineTo(this.center.x, this.canvas.height - 10);
		this.ctx.stroke();
		this.ctx.moveTo(5, this.center.y);
		this.ctx.lineTo(this.canvas.width - 10, this.center.y);
		this.ctx.stroke();
		
		this.ctx.beginPath();
		this.ctx.strokeStyle = "black";
		this.ctx.lineWidth = 0.5;
		var x = this.center.x + this.userRatioX;
		while(x<this.canvas.width){
			this.ctx.moveTo(x, 5);
			this.ctx.lineTo(x, this.canvas.height - 10);
			x = x + this.userRatioX;
		}
		while(x>0){
			this.ctx.moveTo(x, 5);
			this.ctx.lineTo(x, this.canvas.height - 10);
			x = x - this.userRatioX;
		}
		var y = this.center.y;
		while(y<this.canvas.height){
			this.ctx.moveTo(5, y);
			this.ctx.lineTo(this.canvas.width-10, y);
			y = y + this.userRatioY;
		}
		while(y>0){
			this.ctx.moveTo(5, y);
			this.ctx.lineTo(this.canvas.width-10, y);
			y = y - this.userRatioY;
		}
		this.ctx.stroke();
	}

	onBtnDraw(){
		this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
		this.getParams();
		this.drawFunc();
	}
	
	onCtxMouseDown(event){
		if(this.regionFlag)
		{
			this.region.x1 = (event.offsetX*(this.end-this.start)/this.canvas.width)+this.start;
		}
		else
		{
			this.region.x2 = (event.offsetX*(this.end-this.start)/this.canvas.width)+this.start;
		}
		this.regionFlag = !this.regionFlag;
		if(this.region.x1!=this.region.x2){
			this.ctx.putImageData(this.img,0,0);
			this.ctx.strokeStyle = "#ff0000"
			this.ctx.lineWidth = 3;
			this.ctx.beginPath();
			this.ctx.moveTo((this.region.x1-this.start)*this.userRatioX,0);
			this.ctx.lineTo((this.region.x1-this.start)*this.userRatioX,this.canvas.height);
			this.ctx.stroke();
			this.ctx.beginPath();
			this.ctx.moveTo((this.region.x2-this.start)*this.userRatioX,0);
			this.ctx.lineTo((this.region.x2-this.start)*this.userRatioX,this.canvas.height);
			this.ctx.stroke();
			
			let scope={
				x: 0
			}
			if(Math.min(this.region.x1,this.region.x2)<this.x1 && this.x1<Math.max(this.region.x1,this.region.x2))
			{
				scope.x = this.x1-0.001;
				var y1, y2;
				y1 = this.func1.evaluate(scope);
				scope.x = this.x1+0.001
				y2 = this.func2.evaluate(scope);
				if(Math.abs(y1-y2)>0.05)
					this.neprFlag = false;
				else
					this.neprFlag = true;
			}
			else
			{
				this.neprFlag = true;
			}
			
			if(this.neprFlag){
				if(Math.min(this.region.x1,this.region.x2)<this.x2 && this.x2<Math.max(this.region.x1,this.region.x2))
				{
					scope.x = this.x2-0.001;
					var y1, y2;
					y1 = this.func2.evaluate(scope);
					scope.x = this.x2+0.001
					y2 = this.func3.evaluate(scope);
					if(Math.abs(y1-y2)>0.05)
						this.neprFlag = false;
					else
						this.neprFlag = true;
				}
				else
				{
					this.neprFlag = true;
				}
			}
			
			if(this.neprFlag)
				this.parentElement.querySelector('.nepr').innerHTML = "непрерывна";
			else
				this.parentElement.querySelector('.nepr').innerHTML = "прерывна";
		}
		
		
	}
	
	onInputChange(input) {
		switch(input.className){
			case "ratioX":
				
			break;
			case "ratioAutoCheckBox":
				this.autoRatio = input.checked;
				if(this.autoRatio){
					this.parentElement.querySelector('.ratioX').disabled=true;
					this.parentElement.querySelector('.ratioY').disabled=true;
					this.parentElement.querySelector('.ratioCheckBox').disabled=true;
				}else{
					this.parentElement.querySelector('.ratioX').disabled=false;
					this.parentElement.querySelector('.ratioY').disabled=false;
					this.parentElement.querySelector('.ratioCheckBox').disabled=false;
				}
				this.parentElement.querySelector('.ratioCheckBox').checked=false;
			break;
		}	
	}
	
}
